import React from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';


const MetaData = ({ name, shortName, municipality }) => (
  <Paper elevation={3} style={{ marginTop: '20px', width: '400px', padding: '30px' }} >
    <label>Name :</label> {name} <br />
    <label>Short Name :</label> {shortName} <br />
    <label>Municipality :</label> {municipality}
  </Paper>
);

MetaData.propTypes = {
  name: PropTypes.string.isRequired,
  shortName: PropTypes.string.isRequired,
  municipality: PropTypes.string.isRequired
};


export default MetaData;