import React, { useState, useEffect } from 'react';
import axios from 'axios';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Grid from '@material-ui/core/Grid';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';

import MetaData from '../MetaData';
import Observations from '../Observations';

const fp = require('lodash/fp');

const WeatherStation = () => {
  const [weatherStation, setWeatherStation] = useState([]);
  const [options, setOptions] = useState([]);
  const [metaData, setMetaData] = useState('');
  const [observationSourceId, setObservationSourceId] = useState();

  useEffect(() => {
    axios.get('/sources/v0.jsonld?types=SensorSystem&country=NO')
      .then(res => {
        const responseData = fp.flow(
          fp.get('data'),
          fp.slice(0, 30)
        )(res.data);

        setWeatherStation(responseData);

        const options = fp.map(data =>  fp.pick(['id', 'name'], data))(responseData);
        setOptions(options);
      });
  }, []);

  const handleOnchange = (event, value) => {
    const selectedMetaData = fp.find({ id: value.id })(weatherStation);

    setObservationSourceId(selectedMetaData.id);
    setMetaData(selectedMetaData);
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="m">
        <div>
          <Grid container spacing={3} style={{ backgroundColor: '#cfe8fc', height: '100vh' }}>
            <Grid item xs={6}>
              <h4 style={{ textAlign: 'left', marginTop: '80px' }}>
                {fp.isEmpty(weatherStation) ? 'Loading some data. Please wait...' : ''}
              </h4>
              <Autocomplete
                id="combo-box-demo"
                options={options}
                getOptionLabel={(option) => option.name}
                renderInput={(params) =>
                  <TextField {...params} label="Select Weather Station" variant="outlined" />}
                  onChange={handleOnchange}
                style={{ width: '300px' }}
              />
              {!fp.isEmpty(metaData) ? 
              <MetaData
                name={metaData.name} 
                shortName={metaData.shortName}
                municipality={metaData.municipality} />
                : <span />}
            </Grid>
            {!fp.isEmpty(observationSourceId) ? 
              <Grid item xs={6}>
                <Observations 
                  sourceId={observationSourceId} />
              </Grid> : <span />}
          </Grid>
        </div>
      </Container>
    </React.Fragment>
  );
};

export default WeatherStation;