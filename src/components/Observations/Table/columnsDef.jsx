export default [
  {
    key: 'elementId',
    name: 'Temparature'
  },
  {
    key: 'value',
    name: 'Value'
  },
  {
    key: 'unit',
    name: 'Unit'
  },
  {
    key: 'exposureCategory',
    name: 'Category'
  },
  {
    key: 'performanceCategory',
    name: 'Performance'
  }
];
  