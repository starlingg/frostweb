import React, { useState, useEffect } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';

import SimpleTable from './Table';
const fp = require('lodash/fp');

const Observations = ({ sourceId }) => {
  const [toDate, setToDate] = useState('');
  const [fromDate, setFromDate] = useState('');
  const [observations, setObservations] = useState([]);

  const handleOnFromchange = (e) => setToDate(e.target.value);
  const handleOnTochange = (e) => setFromDate(e.target.value);

  useEffect(() => {
    axios.get(`/observations/v0.jsonld?sources=${sourceId}&referencetime=${fromDate}%2F${toDate}&elements=air_temperature%2Cwind_speed%2Cboolean_fair_weather(cloud_area_fraction%20P1D)`)
      .then(res => {
        const responseData = fp.get('data')(res.data);
        setObservations(fp.get('observations')(responseData[0]));
      });
  }, [sourceId, toDate, fromDate]);

  return (
    <React.Fragment>
      <h4
        style={{ textAlign: 'left', marginTop: '80px' }}>
          {fp.isEmpty(toDate) && fp.isEmpty(fromDate) ? 'Please select reference time' : ''}
      </h4>
      <form
        style={{ width: '400px', padding: '30px' }}
        noValidate>
        <TextField
          id="date"
          label="From"
          type="date"
          defaultValue="2017-05-24"
          InputLabelProps={{
            shrink: true,
          }}
          onChange={handleOnFromchange}
        />
        <TextField
          id="date"
          label="To"
          type="date"
          defaultValue="2017-05-24"
          InputLabelProps={{
            shrink: true,
          }}
          onChange={handleOnTochange}
        />
      </form>
      {fp.isEmpty(observations) ?
        <h4 style={{ textAlign: 'left', marginTop: '20px' }}>Please wait..</h4> :
          <SimpleTable details={observations} />}
    </React.Fragment>
  );
};

Observations.propTypes = {
  sourceId: PropTypes.string.isRequired
};


export default Observations;